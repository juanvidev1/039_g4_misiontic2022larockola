-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2022 a las 03:09:55
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `039g4larockola`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artistas`
--

CREATE TABLE `artistas` (
  `id_artista` int(11) NOT NULL,
  `nombre_artista` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `artistas`
--

INSERT INTO `artistas` (`id_artista`, `nombre_artista`) VALUES
(1, 'Carlos Vives'),
(2, 'El Caribefunk'),
(3, 'A Day to Remember'),
(4, 'Karol G'),
(5, 'Chika Di Trujillo'),
(6, 'Diamante Eléctrico'),
(7, 'Tiësto'),
(8, 'J Balvin'),
(9, 'Anuel AA'),
(10, 'Marc Anthony'),
(11, 'Sebastián Yatra'),
(12, 'Shakira'),
(13, 'Christian Nodal'),
(14, 'Luis Barberia'),
(15, 'Chocquibtown'),
(16, 'Marshmello'),
(17, 'Pierce the Veil'),
(18, 'Madonna'),
(19, 'Pipe Bueno'),
(21, 'Ryan Castro'),
(22, 'Maluma'),
(23, 'Jimmi Hendrix');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `canciones`
--

CREATE TABLE `canciones` (
  `id_cancion` int(10) NOT NULL,
  `titulo_cancion` varchar(100) NOT NULL,
  `id_artista` int(10) NOT NULL,
  `id_colaborador` int(10) DEFAULT NULL,
  `id_genero` int(10) NOT NULL,
  `enlace_cancion` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `canciones`
--

INSERT INTO `canciones` (`id_cancion`, `titulo_cancion`, `id_artista`, `id_colaborador`, `id_genero`, `enlace_cancion`) VALUES
(1, 'Provenza', 4, NULL, 3, 'https://www.youtube.com/watch?v=ca48oMV59LU'),
(2, 'Ay, Dios mío!', 4, NULL, 4, 'https://www.youtube.com/watch?v=Ou2c2aj5Fcw'),
(3, 'Don\'t be shy', 4, 7, 4, 'https://www.youtube.com/watch?v=taSubkjZUA4'),
(4, 'Location', 4, 9, 4, 'https://www.youtube.com/watch?v=aw_cmzF_uZY'),
(5, 'No te vayas', 1, NULL, 4, 'https://www.youtube.com/watch?v=FBosdY-h_lo'),
(6, 'El orgullo de mi patria', 1, NULL, 2, 'https://www.youtube.com/watch?v=Yq79ibIx2sc'),
(7, 'Cuando nos volvamos a encontrar', 1, 10, 2, 'https://www.youtube.com/watch?v=Geqmpq0tjNU'),
(8, 'Robarte un beso', 1, 11, 2, ''),
(9, 'Respirar', 2, NULL, 2, 'https://www.youtube.com/watch?v=cq_cz20kRuQ'),
(10, 'Cuatro elementos', 2, NULL, 2, 'https://www.youtube.com/watch?v=nAcxPwAZT3Q'),
(11, 'Alma en su vida real', 2, NULL, 2, 'https://www.youtube.com/watch?v=d52m8DReO9s'),
(12, 'La musa del Caribefunk', 2, 14, 2, 'https://www.youtube.com/watch?v=cxe9QBT3_zQ'),
(13, 'Miracle', 3, NULL, 5, 'https://www.youtube.com/watch?v=9Cb872pCbMs'),
(14, 'Everything we need', 3, NULL, 6, 'https://www.youtube.com/watch?v=nDxOo7kt5Wc'),
(15, 'Nivel de perreo', 6, NULL, 9, 'https://www.youtube.com/watch?v=UMnAlQc8Gw0'),
(16, 'Nivel de perreo', 8, 21, 4, 'https://www.youtube.com/watch?v=DEhNSqEw184');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generos`
--

CREATE TABLE `generos` (
  `id_genero` int(11) NOT NULL,
  `nombre_genero` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `generos`
--

INSERT INTO `generos` (`id_genero`, `nombre_genero`) VALUES
(1, 'Rock en español'),
(2, 'Pop'),
(3, 'Reguetón'),
(4, 'Urbano'),
(5, 'Metalcore'),
(6, 'Rock en inglés'),
(7, 'Vallenato'),
(9, 'Rock Alternativo'),
(10, 'Cumbia'),
(11, 'Música Clásica');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `artistas`
--
ALTER TABLE `artistas`
  ADD PRIMARY KEY (`id_artista`);

--
-- Indices de la tabla `canciones`
--
ALTER TABLE `canciones`
  ADD PRIMARY KEY (`id_cancion`),
  ADD KEY `FK Genero` (`id_genero`),
  ADD KEY `FK Artista` (`id_artista`),
  ADD KEY `FK Artista colaborador` (`id_colaborador`);

--
-- Indices de la tabla `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id_genero`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `artistas`
--
ALTER TABLE `artistas`
  MODIFY `id_artista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `canciones`
--
ALTER TABLE `canciones`
  MODIFY `id_cancion` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `generos`
--
ALTER TABLE `generos`
  MODIFY `id_genero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `canciones`
--
ALTER TABLE `canciones`
  ADD CONSTRAINT `FK Artista` FOREIGN KEY (`id_artista`) REFERENCES `artistas` (`id_artista`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK Artista colaborador` FOREIGN KEY (`id_colaborador`) REFERENCES `artistas` (`id_artista`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK Genero` FOREIGN KEY (`id_genero`) REFERENCES `generos` (`id_genero`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
