
function loadData(){
    let request = sendRequest('artistas/list', 'GET', '')
    let table = document.getElementById('artists-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        data.forEach(element => {
            table.innerHTML += `
            <tr>
                <th>${element.idArtista}</th>
                <td>${element.nombreArtista}</td>
                <td>
                    <button type="button" class="btn btn-primary" onclick='window.location = "./form_artistas.html?id=${element.idArtista}"'>Editar</button>
                    <button type="button" class="btn btn-danger" onclick='deleteArtist(${element.idArtista})'>Eliminar</button>
                </td>
            </tr>

            `
            
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function deleteArtist(idArtista){
    let request = sendRequest('artistas/'+idArtista, 'DELETE', '');
    request.onload = function(){
        loadData();
    }
}

function loadArtist(idArtista){
    let request = sendRequest('artistas/list/'+idArtista, 'GET', '');
    let id = document.getElementById('artist-id');
    let artistName = document.getElementById('artist-name');
    request.onload = function(){
        let data = request.response;
        id.value = data.idArtista
        artistName.value = data.nombreArtista
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }
    
}

function saveArtist(){
    let artistId = document.getElementById('artist-id').value
    let artistName = document.getElementById('artist-name').value
    let data = {'idArtista': artistId, 'nombreArtista': artistName}
    let request = sendRequest('artistas/', artistId ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'artistas.html';
    }
    request.onerror = function(){
        alert("Error al guardar los cambios");
    }
}

