function loadData(){
    let request = sendRequest('generos/list', 'GET', '')
    let table = document.getElementById('genre-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        data.forEach((element,index) => {
            table.innerHTML += `
            <tr>
                <th>${element.idGenero}</th>
                <td>${element.nombreGenero}</td>
                <td>
                    <button type="button" class="btn btn-primary" onclick='window.location = "./form_generos.html?id=${element.idGenero}"'>Editar</button>
                    <button type="button" class="btn btn-danger" onclick='deleteGenre(${element.idGenero})'>Eliminar</button>
                </td>
            </tr>

            `
            
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function deleteGenre(idGenero){
    let request = sendRequest('generos/'+idGenero, 'DELETE', '');
    request.onload = function(){
        loadData();
    }
}

function loadGenre(idGenero){
    let request = sendRequest('generos/list/'+idGenero, 'GET', '');
    let id = document.getElementById('genre-id');
    let genreName = document.getElementById('genre-name');
    request.onload = function(){
        let data = request.response;
        id.value = data.idGenero
        genreName.value = data.nombreGenero
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }
    
}

function saveGenre(){
    let genreId = document.getElementById('genre-id').value
    let genreName = document.getElementById('genre-name').value
    let data = {'idGenero': genreId, 'nombreGenero': genreName}
    let request = sendRequest('generos/', genreId ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'generos.html';
    }
    request.onerror = function(){
        alert("Error al guardar los cambios");
    }
}
