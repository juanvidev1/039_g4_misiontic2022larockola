function loadSongData(){
    let request = sendRequest('canciones/list', 'GET', '')                                                                                                               
    let table = document.getElementById('songs-table');
    table.innerHTML = "";
    // El orden de los <th> es el orden en el que la tabla mostrará los datos de la base de datos
    request.onload = function(){
        let data = request.response;
        data.forEach(element => {
            table.innerHTML += `
            <tr>
                <th>${element.idCancion}</th>
                <td>${element.nombreCancion}</td>
                <td>${element.artista.nombreArtista}
                <td>${element.genero.nombreGenero}</td>
                <td>${element.idColaborador}</td>
                <td><a href="${element.enlaceCancion}">Escucha</a></td>
                <td>
                    <button type="button" class="btn btn-primary" onclick='window.location = "./form_canciones.html?id=${element.idCancion}"'>Editar</button>
                    <button type="button" class="btn btn-danger" onclick='deleteSong(${element.idCancion})'>Eliminar</button>
                </td>
            </tr>

            `
            
        });

        
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function deleteSong(idCancion){
    let request = sendRequest('canciones/'+idCancion, 'DELETE', '');
    request.onload = function(){
        loadSongData();
    }
}

function loadSong(idCancion){
    let request = sendRequest('canciones/list/' + idCancion, 'GET', '');
    let id = document.getElementById('song-id');
    let songName = document.getElementById('song-name');
    let artist = document.getElementById('artist-id');
    let genre = document.getElementById('genre-id');
    let colaborador = document.getElementById('colaborator-id');
    let songLink = document.getElementById('youtube-link');

    request.onload = function(){
        let data = request.response
        id.value = data.idCancion
        songName = data.nombreCancion
        artist.value = data.artista.idArtista
        genre.value = data.genero.idGenero
        colaborador.value = data.idColaborador
        songLink.value = data.enlaceCancion
    }
}

function selectArtist() {
    let request2 = sendRequest('artistas/list', 'GET', '')
    var select = document.getElementById('lista');  
    var mOptions = "";
    var selectOpt ="";
    request2.onload = function(){
    let data = request2.response; 
       /*Variable para concatenar los option*/   
       data.forEach((element) => {
           mOptions += `<option value="${element.idArtista}">${element.idArtista} ${element.nombreArtista}</option>`; 
       }); 
       /*Modificamos el select una vez, n100 o veces*/
       select.insertAdjacentHTML('beforeend', mOptions); 
      
    var dato = document.getElementById('lista').addEventListener('click', function(){
       selectOpt = select.options[select.selectedIndex];
       });
    }  
       
    select.insertAdjacentHTML('beforeend', mOptions);
       
}

function selectGenero(){
    let request3 = sendRequest('generos/list', 'GET', '')
    var select1 = document.getElementById('genres-list');
    var nOptions = "";
    var selectedOption = "";
    request3.onload = function(){
        let data = request3.response;

        data.forEach((element) => {
            nOptions += `<option value="${element.idGenero}">${element.idGenero} ${element.nombreGenero}</option>`;
        });

        select1.insertAdjacentHTML('beforeend', nOptions);

        var dato1 = document.getElementById('genres-list').addEventListener('click', function(){
            selectedOption = select1.options[select1.selectedIndex];
        });
    }

    select1.insertAdjacentHTML('beforeend', nOptions);
}

function saveSong(){
    let id = document.getElementById('song-id').value
    let songName = document.getElementById('song-name').value
    let artist = document.getElementById('artist-id').value
    let genre = document.getElementById('genre-id').value
    let colaborador = document.getElementById('colaborator-id').value
    let songLink = document.getElementById('youtube-link').value
    let data = {
        'idCancion': id,
        'artista': {'idArtista': artist},
        'genero': {'idGenero': genre},
        'idColaborador': colaborador,
        'nombreCancion': songName,
        'enlaceCancion': songLink
    }

    let request = sendRequest('canciones/' + id, 'POST', data)
    
    request.onload = function(){
        window.location = 'canciones.html';
    }

    request.onerror = function(){
        alert("Error al guardar la canción");
    }
}

function updateSong(){
    let id = document.getElementById('song-id').value
    let songName = document.getElementById('song-name').value
    let artist = document.getElementById('artist-id').value
    let genre = document.getElementById('genre-id').value
    let colaborador = document.getElementById('colaborator-id').value
    let songLink = document.getElementById('youtube-link').value
    let data = {
        'idCancion': id,
        'artista': {'idArtista': artist},
        'genero': {'idGenero': genre},
        'idColaborador': colaborador,
        'nombreCancion': songName,
        'enlaceCancion': songLink
    }

    let request = sendRequest('canciones/', 'PUT', data)
    
    request.onload = function(){
        window.location = 'canciones.html';
    }

    request.onerror = function(){
        alert("Error al guardar la canción");
    }  
}



